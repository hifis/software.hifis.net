---
title: Roadmap
redirect_to: https://hifis.net/roadmap
---
{% comment %}
This page redirects the visitor to the HIFIS Roadmap which covers 
our HIFIS Software roadmap items as well as roadmaps from all other 
HIFIS clusters.
{% endcomment %}
