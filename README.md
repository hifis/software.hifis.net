# HIFIS Software web page

The HIFIS Software web page will be the home for gathering information about
the activities and services offered by the HIFIS Software team. This website
is built using Jekyll.

## Build the project locally

1. Clone the project locally
   ```bash
   git clone --recursive https://gitlab.hzdr.de/hifis/software.hifis.net.git
   cd software.hifis.net
   ```
2. [Install](https://jekyllrb.com/docs/installation/) Jekyll.
3. Install dependencies.
   ```bash
   bundle install
   ```
4. Build and preview the web page.
   ```bash
   bundle exec jekyll serve --future
   ```
5. Add content.

Execute the commands above from the root of your repository.

More information is available in Jekyll's
[documentation](https://jekyllrb.com/docs/)

## Add yourself as a team member

There are basically two variants of achieving this:

### Do it yourself

1. [Git-clone](https://git-scm.com/docs/git-clone) the repository and 
[Git-checkout](https://git-scm.com/docs/git-checkout) into a new branch 
(with option `-b <new-branch-name>`)
2. Edit [_data/hifis_team.yml](_data/hifis_team.yml) and add your information.
3. Add `<your_id_here>.md` into the `_members` folder, containing at least the 
following frontmatter:
```yaml
---
layout: member

title: <your_last_name>, <your_first_name>
data: <your_id>
---
```
4. Create a 
[merge request](https://docs.gitlab.com/ce/gitlab-basics/add-merge-request.html)
in GitLab.
5. Let someone review and merge your changes into branch `master`.

### Let someone else do it :)

1. Create a new issue, using the _add team member_ template.
2. Fill in at least the required information.
