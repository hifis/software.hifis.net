---
title: "Introducing: Consulting Services"
date: 2020-08-19
authors:
  - frere
layout: blogpost
title_image: headway-5QgIuuBxKwM-unsplash.jpg
categories:
  - announcement
tags:
  - general
  - consulting
excerpt: >
    HIFIS Software Consulting Services offers free-of-charge consulting services to all research groups within the Helmholtz umbrella.
    Find out what that means, and how you can get software support for your project.
redirect_from:
  - announcement/2020/08/19/introducting-consulting/
redirect_to: https://hifis.net/announcement/2020/08/19/introducing-consulting
---


# Consulting Services

As programming and coding increasingly become important tools in the scientist's toolbox,
it also becomes increasingly common to run into bigger software issues.
Issues like:

* How do I get this old library to run on our new HPC cluster?
* How do I release my simulation tool so that it can be reviewed along with the rest of my paper?
* How can I track my code, dependencies, and data so that I can be sure that people can use this project even after I've finished working with it?

HIFIS Software Consulting Services offers free-of-charge consulting services to all research groups within the Helmholtz umbrella.
Our aim is to provide software development experts who can answer questions and provide support for all topics in the areas of research and software.

## Case in Point

When I joined the HIFIS team, one of my first consultation projects was at HZDR,
with a group whose code would only run on one particular server that they could not continue maintaining.
By the end of our involvement, we had:

* Set up _CMake_ to manage the project's dependencies;
* Set up their project (via Gitlab) to automatically build the code every time the team made a change, so that they could see quickly if something was breaking;
* Made the software run on the HZDR-wide HPC infrastructure;
* Documented the installation process so that future team members would know how to get started quickly.

In other cases, we've been able to
look into data protection issues for a team setting up a website for their project,
and advise a spin-off company on technology choices.

## Get Started

If any of this sounds like something that might help you or your project, get in touch!
HIFIS Consulting is free, and available to any group or team that falls under the Helmholtz umbrella.
All you need to do is fill out the form on our [**consulting page**][hifis/consulting],
and we will be in touch as soon as possible.

[hifis/consulting]: {% link services/consulting.html %}

<br />

<div class="alert alert-success">
  <h2 id="contact-us"><i class="fas fa-info-circle"></i> Get In Touch</h2>
  <p>
    If you work for a Helmholtz-affilliated institution, and think that something like this would be useful to you, send us an e-mail at
    <strong><a href="mailto:{{site.contact_mail}}">{{site.contact_mail}}</a></strong>,
    or fill in our
    <strong><a href="{% link services/consulting.html %}#consultation-request-form">consultation request form</a></strong>.
  </p>
</div>
