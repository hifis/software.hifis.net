---
title: The Software Policy Template
date: 2020-01-28
authors:
  - huste
layout: blogpost
title_image: mountains-nature-arrow-guide-66100.jpg
excerpt:
  <p>In a joint effort of the <em>Task Group Research Software</em> 
  within the scope of the <em>Helmholtz Open Science Office</em> 
  a software policy template was created.</p>
  <p>This model policy should serve as a guideline and reusable blueprint for
  the creation of regulations for a sustainable use and development of
  research software in the individual Helmholtz centers.</p>
categories:
  - guidelines
tags:
  - general
redirect_to: https://hifis.net/guidelines/2020/01/28/software-policy-template
---

In a joint effort of the [Task Group Research Software][taskgroup] within the
scope of [Helmholtz Open Science][openscience] a
[software policy template][policy][^1] was created and published.
This model policy should serve as a guideline and reusable template for the
creation of regulations for a sustainable use and development of research
software in the individual Helmholtz centers.
The template is accompanied by a [position paper][recommendations][^2] providing
recommendations for the implementation of these guidelines for the treatment of
research software at the Helmholtz centers.

## How Do These Guidelines Help Me as a Researcher?
The processing of research data and the acquisition of scientific knowledge
require the development and use of high-quality and sustainable software
solutions. 

It is clear that the verifiability and reproducibility of scientific
results can only be guaranteed if research software is treated under
defined conditions.

Such software is to be considered a central and independent scientific
product. 
It needs to be handled according to similar high-level standards that
is already applied for other scientific methods.

The software policy template is here to help your research center to define
standards, guidelines, best practices as well as affirmative actions to give
sustainable research software development the significance it deserves.
At the same time it supports _you_ by defining guidelines and rules that you can
rely upon.
It also aids in promoting the use of software development best practices by
providing you with the appreciation you deserve.

## How Will HIFIS Support the Implementation of This Template?
HIFIS will provide practical information for you, as a researcher, on how to
implement the requirements in your research projects.
At the same time, the educational program will also be aligned with the
requirements defined in this template.
HIFIS would also be happy to guide the individual centers in transforming the
template into local guidelines.

## Referenced Documents
[^1]: The Software Policy Template;
      [<i class="fas fa-file-pdf"></i> 10.2312/os.helmholtz.007][policy-doi]
      (currently only available in German)

[^2]: The position paper "Recommendations for the Implementation of Guidelines
      for the Treatment of Research Software at the Helmholtz Centres";
      [<i class="fas fa-file-pdf"></i> 10.2312/os.helmholtz.008][recommendations-doi]
      (currently only available in German)

[taskgroup]: https://os.helmholtz.de/de/open-science-in-der-helmholtz-gemeinschaft/akteure-und-ihre-rollen/arbeitskreis-open-science/task-group-forschungssoftware/
[openscience]: https://os.helmholtz.de
[policy]: https://os.helmholtz.de/index.php?id=3951
[policy-doi]: https://doi.org/10.2312/os.helmholtz.007
[recommendations]: https://os.helmholtz.de/index.php?id=3197
[recommendations-doi]: https://doi.org/10.2312/os.helmholtz.008
