---
title: The Team
title_image: default
layout: team
excerpt: A list of HIFIS team members and associates.
redirect_to: https://hifis.net/team
---
{% comment %}
  This markdown file triggers the generation of the team page.
  Only the frontmatter is required by Jekyll.
  The contents section does not get rendered into HTML on purpose.
{% endcomment %}
