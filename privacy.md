---
title: Privacy
layout: default
---

# Privacy Policy
Helmholtz-Zentrum Dresden-Rossendorf e. V. (HZDR) takes the protection of 
personal data seriously.

The following privacy policy provides you with information on what personal data
we (the controller as defined in the General Data Protection Regulation) 
collect when you visit our website _software.hifis.net_.
Personal data includes all data with which you can be personally identified. 
The website is hosted on an internal HZDR server.

## Protocols, Log Files

When users access our website, data such as the computer’s IP address, 
internet browser used, date, and time are automatically logged for technical 
reasons and saved for a period of seven days in our system log files. 
Saving in log files occurs to ensure the functionality of the website and the 
security of our information technology systems. 
This data is therefore only evaluated when technical problems or attacks on our
webserver arise. 
The legal basis for data storage is 
_Article 6, Paragraph 1 f GDPR (legitimate interest)_. 
Combining this information with other user data does not occur.
We do not use analysis tools.

We in no way save or gather personal data through this website 
(such as names, addresses, telephone numbers or email addresses) unless you 
provide us with such information.

All personal data is deleted or blocked as soon as the purpose of the storage
no longer applies.
Furthermore, storage may also take place to the extent it is permitted by
European or national legislation.

We use SSL encryption so that the data you enter on our homepage is
transferred to us in encrypted form. 
Users should, however, be aware that Internet data transfer
(e.g., through email communication) can encounter security issues.
Complete protection from access by third parties is impossible.

## Links to External Offers

We link to external websites as well as to social media providers
(Facebook, Twitter, YouTube, Instagram). 
The connection is set up in such a way that no data is transmitted to the
provider of external offers when visiting our internet pages.

When clicking on a link or button, you will, however, be sent directly to that 
external offer.
These sites collect and process your data — particularly log data
(including IP address, browser type, operating system, previously accessed
website, accessed pages, cookie information — according to their own user
guidelines and/or privacy polices, which you can view on the respective sites:

  * Twitter: https://twitter.com/privacy?lang=de
  * YouTube (Google): https://policies.google.com/privacy?hl=en
  * Instagram (Facebook): https://help.instagram.com/519522125107875

We recommend that you read these statements before using the sites.
Please be aware that data protection regulations in regard to those services
may be updated or changed regularly due to extended functionalities or similar
reasons.
We have no influence on the type and scope of data processing by the provider
based on your click. Information regarding your respective rights and settings
for protecting your privacy can be found in the respective privacy policies.

## Your Rights / Contact Information for Our Data Protection Representative

For all data protection matters and, in particular, to exercise your rights
pursuant to
_Art. 15 and subsequent articles of the GDPR (information on data
processed about you, its origin, recipient and the purpose of processing and,
where applicable, a right to correction, restriction of processing or deletion
and revocation or objection)_,
you can contact our data protection representative.

```
  Kristin Beyer (LL.M.)
  Dresdner Institut für Datenschutz
  Email: dsb[at]hzdr.de
```

Additional contact data can be found at: [dresden-datenschutz.de](https://dresden-datenschutz.de/)

Your concerns will be treated with confidentiality.

Furthermore, if you do not consent to our data processing, you can contact the
supervisory authority (Saxon data protection representative, 
[www.saechsdsb.de](https://www.saechsdsb.de))
at any time.

In case of legal disputes, only the German version of this text is legally
binding.
