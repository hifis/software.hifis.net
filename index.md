---
title: HIFIS Software
title_image: default
layout: frontpage
additional_css:
    - frontpage.css
    - title/service-title-buttons.css

additional_js: frontpage.js
excerpt:
  "HIFIS Software aims to make research software engineering more
  sustainable."
redirect_to: https://hifis.net
---
{% comment %}
  This markdown file triggers the generation of the frontpage.
  Only the frontmatter is required by Jekyll.
  The contents section does not get rendered into HTML on purpose.
{% endcomment %}
