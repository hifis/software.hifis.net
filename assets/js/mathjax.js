---
---
{% assign mathjax_path = site.directory.vendor | strip | append: "MathJax/" %}

document.addEventListener('DOMContentLoaded', function () {
    var script = document.createElement('script');
    script.src = "{{ mathjax_path | append: 'es5/tex-mml-chtml.js' | relative_url }}";
    script.defer = true;
    document.head.appendChild(script);
}, false);
