---
title: <i class="fas fa-question-circle"></i> FAQ - Frequently Asked Questions
layout: default
excerpt:
    Collection of Frequently Asked Question (FAQ) about HIFIS Software.
redirect_to: https://hifis.net/faq
---

## General
{:.text-success}
#### [What is the mission of HIFIS Software?][1]
HIFIS Software Services mission is to empower scientists of any domain to
implement and to perpetuate modern scientific software development principles
in order to make research software engineering more sustainable.

#### [How can HIFIS Software help me as a researcher?][2]
Developing software is an essential part of a modern research process.
Reproducible high-quality research requires a sustainable software devlopment
process. HIFIS Software helps you with four different components:
- [Education & Training](services/training)
- [Technology Services](services#technology)
- [Consulting Services](services/consulting)
- [Community Services](services#community)

#### [Whom can I talk to about HIFIS Software in my Helmholtz center?][3]
Visit the [team page](team) for getting contact information. If there is no
HIFIS Software associate in your center yet, we are happy to help to find
and nominate someone in your institute as [contact person][contact].

#### [My Helmholtz center is not directly involved into HIFIS Software. Do you still help me?][4]
Yes, of course. HIFIS in general is a Helmholtz-wide platform that aims to
provide offers for **all** Helmholtz centers.

#### [Is there more in HIFIS than the Software Cluster?][5]
Yes, there exist two more clusters within HIFIS:

- *Backbone Services* - provide a stable network infrastructure and jointly
  usable core services in order to meet the increasing demand for networking 
  of research and the increasing volume of data in the Helmholtz Association
- *Cloud Services* - offer a federated cloud platform
  to the entire scientific community and partners in the long tail of
  science

Visit [www.hifis.net](https://www.hifis.net) for general information about
HIFIS.

#### [How should I acknowledge HIFIS assistance in a paper?][10]

Please include something like this:

> We gratefully acknowledge the HIFIS.net (Helmholtz Federated IT Services) 
> Software team for ... (e.g. support with the [components above][2])

---
## Education & Training
{:.text-success}
#### [Which trainings are you able to offer?][6]
Please have a look into our training catalogue. This is a collection of
material that either is developed by external organizations, e.g.
[The Carpentries](https://carpentries.org/), originates from various Helmholtz
centers or is created by HIFIS Software itself.

#### [Where do I find upcoming training events?][7]
Our [events page](events) lists future events.

#### [I need a software training for my research group. Can you provide one?][8]
We are very happy to provide trainings for you. Please have a look into our
[events page](events) first, to see future scheduled trainings. If there is no
event that fits your needs, please [contact us][contact].

---
## Technology Services
{:.text-success}
#### [When are first Technology Services available for use?][9]
It is planned to have the first usable services available by mid 2020. You can
visit the [Technology Services](services#technology) page for further
information.

[1]: #what-is-the-mission-of-hifis-software
[2]: #how-can-hifis-software-help-me-as-a-researcher
[3]: #whom-can-i-talk-to-about-hifis-software-in-my-helmholtz-center
[4]: #my-helmholtz-center-is-not-directly-involved-into-hifis-software-do-you-still-help-me
[5]: #is-there-more-in-hifis-than-the-software-cluster
[6]: #which-trainings-are-you-able-to-offer
[7]: #where-do-i-find-upcoming-training-events
[8]: #i-need-a-software-training-for-my-research-group-can-you-provide-one
[9]: #when-are-first-technology-services-available-for-use
[10]: #how-should-i-acknowledge-hifis-assistance-in-a-paper
[contact]: mailto:{{ site.contact_mail }}
