---
title: Services
title_image: default
layout: services/index
author: none
additional_css:
    - services/custom.css
    - title/service-title-buttons.css
excerpt:
  "The service portfolio of HIFIS Software is structured into 4 different
  components that seamlessly interoperate with each other: Education &
  Training, Technology, Consulting and Community Services."
redirect_to: https://hifis.net/services#software
---
{% comment %}
  This markdown file triggers the generation of the services page.
  Only the frontmatter is required by Jekyll.
  The contents section does not get rendered into HTML on purpose.
{% endcomment %}
