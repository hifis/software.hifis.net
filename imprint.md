---
title: Imprint
layout: default
---
{% if site.baseurl != '/' %}
{% assign base_url = site.baseurl | append: '/' %}
{% else %}
{% assign base_url = site.baseurl %}
{% endif %}

Helmholtz-Zentrum Dresden - Rossendorf e. V.  
Bautzner Landstraße 400  
01328 Dresden

Phone: +49 351 260 - 0  
Fax: +49 351 269 - 0461  
E-Mail: [kontakt@hzdr.de](mailto:kontakt@hzdr.de)  
Web: [https://www.hzdr.de](http://www.hzdr.de)

---
#### Board of Directors
**Scientific Director & Spokesperson**  
Prof. Dr. Sebastian M. Schmidt

**Administrative Director**  
Dr. Diana Stiller

---

Registration Court:  District Court Dresden  
Register Number:  VR 1693

Turnover Tax Identification Number pursuant to § 27a Turnover Tax Law:  DE 140213784

---

#### HZDR Website Contents and Links: Liability & Disclaimer
As a service provider, we are responsible for the contents of our website in
accordance with the general laws and regulations pursuant to § 7, Sec. 1 of
the German Telemedia Act (TMG).  The contents of our web pages have been
composed with the utmost care and diligence.  However, we cannot assume any
liability for the accuracy, completeness, and timeliness of the information
contained on our website.

Despite careful monitoring, we cannot be held liable for the contents of any
links to external websites of third parties. These websites and their
contents shall be subject to the liability of the respective website
operator(s). Please report any content and technical errors you may
have found on our website via our public
[issue tracker](https://gitlab.hzdr.de/hifis/software.hifis.net/issues).

---

#### Further Information
  * [Privacy Policy]({% link privacy.md %})
  * [Licensing]({% link LICENSE.md %})
