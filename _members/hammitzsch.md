---
layout: member

title: Hammitzsch, Martin
data: hammitzsch
---
# About

I have been involved in software development projects in different roles in research and the private sector since the late nineties. 
Together with other enthusiastic and committed peers I helped to establish the [RSE community](https://de-rse.org/) in the German research landscape 
and supported the founding of ["de-RSE e.V. - Society for Research Software"](https://de-rse.org/de/association.html). 
In the context of the Helmholtz association, I am active in the task group ["Research Software"](https://os.helmholtz.de/de/open-science-in-der-helmholtz-gemeinschaft/akteure-und-ihre-rollen/arbeitskreis-open-science/task-group-forschungssoftware/) as part of the _Open Science_ working group. 
Moreover, colleagues and me are responsible for establishing policies and processes associated with research software as part of GFZ's working group _Software Development_. 
So I am excited that research software is in focus of our work across the Helmholtz centres with the HIFIS Software Services to help scientists in accomplishing their research challenges.

# Interests

* eScience Platforms
* Research Software Engineering
* Research Data Management

# Responsibilities within [HIFIS Software Cluster](https://www.hifis.net/mission/cluster-software.html)

Contact person at GFZ for HIFIS in general and the HIFIS Software Services specifically.
