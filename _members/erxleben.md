---
layout: member

title: Erxleben, Fredo
data: erxleben
---

## About

I am a research software engineer, who loves to teach and learn in equal
measure.

## Working Expertise

These are the primary fields I have worked in so far:

* Digital Circuit Design and Implementation
* FPGAs
* Low-level languages
* Hardware-Software Co-design
* Software Tools and Tooling Software
* User interfaces, User experience design
* Teaching and Education
