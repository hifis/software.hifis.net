---
layout: member

title: Dolling, Maximilian
data: dolling
---

## About
I was interested in computer and what they are capable of, since I turned one on for the first time.
That led to studies in environmental informatics.
I was self employed the last years and worked for small start-ups up to global companies.
I am new to the world of research, but was always interested in it.
Now I am responsible for workshops in software development, software licenses and community management.
I participate as much as I can in [RSE](https://de-rse.org/)-events and believe that we can do something big here.

## Interests
Good research is (partly) based on good research software.
To maximize quality and quantity of research outcome, I want to support researchers to develop software,
that has a certain quality and inherits the [FAIR](https://content.iospress.com/articles/data-science/ds190026) principles.

* Open Science
* Research Software Engineering
* System and Process design and optimization


## Responsibilities within HIFIS Software Cluster

* Education and training on
    * Version control systems
    * Programming basics
* Consulting on
    * Best practices
    * Software licensing
    * Software architecture
    * Requirements engineering
* Community services
    * Build up and support communities
