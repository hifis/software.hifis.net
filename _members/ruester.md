---
layout: member

title: Rüster, Matthias
data: ruester
---

## Responsibilities within HIFIS Software Cluster

* Education and training (Instructor and helper for software carpentry trainings)
