---
layout: member

title:  Brinckmann, Nils
data:   brinckmann
---

## About

* Bachelor in Earth Sciences
* Master in Geoinformation Science & Visualization
* Former student assistant in a joint PIK & World Bank project to analyze poverty development based on climate change impacts
* Former software engineer & data scientist at EMANO Development in automotive industry
* Since january 2019 full stack software developer at GFZ

## Interests

* Machine learning
* Geospatial data processing
* Database technologies
* Programming languages
* Interdisciplinary projects

## Responsibilities within HIFIS Software Cluster

* Education and training (Instructor and helper for software carpentry trainings)
