---
layout: member

title: Hüser, Christian
data: hueser
---


## About me

Hi,

my name is Christian Hüser and I am _Research Software Engineer_ at the
_Helmholtz-Zentrum Dresden-Rossendorf_ and working on _HIFIS Software_ 
since September 2019. 
I am interested in Scientific Software, in particular Simulation and
Scientific Computing Software since my studies of 
_Applied Systems Science_, which is about mathematical model building, 
computer simulations of these models and analysing those results
from model simulations. 

I am a full-stack software developer and working in open source
software development projects being in charge for the management
of the whole application lifecycle. 
I am a proponent of Clean Code and Architecture and of sustainable 
software development processes.
For me software modelling and software quality assurance measures
like software refactoring and testing as well as Continuous Integration
and Automation is an integral part of my work.

In my spare time I am doing sports, music, board games and vegan
cooking and baking.
I am an environmental activist fighting for a better future.

## Biography

* Studied _Applied Systems Science_ at the _University of Osnabrück_ and 
graduated in 2011.
* Software Developer at _BIG 5 Concepts GmbH_ in Osnabrück for 3 years.
* Software Developer at _PHYWE Systeme GmbH & Co. KG_ in Göttingen for 
3 years.
* Software Developer at _Westfälische Wilhelms-Universität Münster_ for 
2 years.
* Current position is _Research Software Engineer_ at the 
_Helmholtz-Zentrum Dresden-Rossendorf_ working on platform 
_HIFIS Software_ since September 2019.

## Responsibilities in _HIFIS Software_

This is an excerpt of my services which I am offering:

* Object-oriented and Component-based Software 
Modelling and Architecture
* Web-Development and Web-Services
* Software Development Frameworks
* Database Development and Data Modelling
* Software Testing and Software Quality Assurance Measures
* Software Reviews and Refactoring
* Version Control with Git and Git LFS
* Version and Project Management with GitLab and GitHub
* Automation and GitLab Continuous Integration Pipelines
* Docker Container and GitLab Container Registry
* Model Building, Scientific Computing and Simulation Software
* There is more to come ...

So far I am working on the following applications in _HIFIS Software_:

* HIFIS Software Web-Page
* Containerized MediaWiki
* Software Platforms promoting Open Science
* There is more to come ...
