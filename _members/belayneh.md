---
layout: member

title:  Belayneh, Bezaye Tesfaye
data:   belayneh
---

## About
I have a background in Computer Science and Geoinformatics.
I have experience in developing GIS systems from back-end to front-end, and I have been working on developing efficient index structures that allow fast queries in large graphs representing real world systems like transportation networks.
I am also interested to learn more and share my experience on tools and techniques that are relevant for reproducible software in research.

## Interests
* Spatio-temporal database
* Transportation network
* Graphs
* Path queries
* Reproducible research software


## Responsibilities within HIFIS Software Cluster

* Education and training on
    * Tools for collaborative research
    * Version control systems and
    * Scripting languages
* Consulting on
    * Best practices
    * Software licensing and
    * Reproducible software
