#! /usr/bin/env bash

# ==============================================================================
# title       : create_jumbotron_variants.sh
# description : Iterates over all image files in a given folder and creates
#               smaller variants of these according to given breakpoints.
#               The smaller versions are sorted into subfolders according to the
#               breakpoint they were generated for.
# date        : 2019-11-07
# requires    : Imagemagick v6 or compatible
# usage       : $ bash ./scripts/create_jumbotron_variants assets/img/jumbotron/
# parameters  : (1) path to the assets subfolder containing the jumbotron images
#               to be processed.
# notes       : The intention to outsource this script is to collect all shell
#               scripts in a separate scripts folder instead of putting all
#               scripts with all its length into the GitLab CI pipeline
#               configuration yaml file.
#               Beside that users can manually call this script as well.
# ==============================================================================

# Check if ImageMagick programs are available
if ! [ -x "$(command -v identify)" ]
then
  echo 'ERROR: Command \"identify\" required (provided by imagemagick v6)' >&2
  exit 1
fi

if ! [ -x "$(command -v convert)" ]
then
  echo 'ERROR: Command \"convert\" required (provided by imagemagick v6)' >&2
  exit 1
fi

# Check whether $1 is sane
if ! [ -d "$1" ]
then
    echo "ERROR: $1 was expected to be a valid directory" >&2
    exit 1
fi

# The list of breakpoints for which to generate variants and their widths in px
declare -A breakpoints=(\
    ["phone"]="600"\
    ["tablet"]="1200"\
    ["desktop"]="1800"\
    ["display_2k"]="2400"\
)

# From now on we work in the folder given by $1
cd "$1" || exit

for bp_name in "${!breakpoints[@]}"
do echo "$bp_name - ${breakpoints[$bp_name]}"

    # If the target folders do not exist, create them
    if ! [ -d "$bp_name" ]
    then
        echo "Creating folder $bp_name"
        mkdir "$bp_name"
    fi

done

# Get all the images to convert (ignore subfolders)
images=$(find . -maxdepth 1 -iregex ".*\.\(jpg\|gif\|png\|jpeg\|webp\)")

for source_file in $images
do
    image_width=$(identify -format "%[fx:w]"  "$source_file")
    echo "=== Processing $source_file (width = $image_width px) ==="

    for bp_name in "${!breakpoints[@]}"
    do
        target_file=$bp_name/$source_file
        target_width=${breakpoints[$bp_name]}

        if [ "$image_width" -gt "$target_width" ]
        then
            # Do not overwrite existing files
            if [ -f "$target_file" ]
            then
                echo "Skip $target_file (already exists)"
                continue
            else
                # Create new version for this breakpoint
                echo "Generating $target_file ($target_width px)"
                convert "$source_file" -resize "$target_width" "$target_file"
            fi
        else
            # Instead of upscaling, create a relative link to the original
            echo "Creating link $target_file (Source smaller then target)"
            ln -sr "$source_file" "$target_file"
        fi
    done
done
exit 0

