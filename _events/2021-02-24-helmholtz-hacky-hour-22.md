---
title: "Helmholtz Hacky Hour #22"
layout: event
organizers:
  - dolling
type:      hacky-hour
start:
  date:   "2021-02-24"
  time:   "14:00"
end:
  time:   "15:00"
location:
  campus: "virtual"
  room:   "<a href=https://meet.gwdg.de/b/max-93j-2ef><i class=\"fas fa-external-link-alt\"></i> meet.gwdg.de</a>"
excerpt:  "<strong>What could go wrong? Horror Stories in (collaborative) scientific computation</strong> The moment they realized their work of the past years was almost wasted..."
redirect_to: https://hifis.net/events/2021/02/24/helmholtz-hacky-hour-22
---
## What could go wrong? Horror Stories in (collaborative) scientific computation
The Hacky Hour is intended to discuss and learn about different topics in the context of research software development. During the session, you will meet Helmholtz researchers from different fields and have the opportunity to present your favorite tools and techniques. If you want to join, share your experience or have any question on the topic, let us and others know about it in [the meta pad](https://pad.gwdg.de/0HczFKgqS_C9L1QGzfpbJA#).

Everyone is invited to share their worst experiences during work with computers and other alien lifeforms, and how to avoid them in the future (if possible).

We are looking forward to seeing you!
