---
title: "Helmholtz Hacky Hour #23"
layout: event
organizers:
  - dolling
type:      hacky-hour
start:
  date:   "2021-03-10"
  time:   "14:00"
end:
  time:   "15:00"
location:
  campus: "virtual"
  room:   "<a href=https://meet.gwdg.de/b/max-93j-2ef><i class=\"fas fa-external-link-alt\"></i> meet.gwdg.de</a>"
excerpt:  "<strong>How to select the right tools in software development?</strong> How to avoid not to get lost inside multiple tools available when choosing for software development?"
redirect_to: https://hifis.net/events/2021/03/10/helmholtz-hacky-hour-23
---
## How to select the right tools in software development?
The Hacky Hour is intended to discuss and learn about different topics in the context of research software development. During the session, you will meet Helmholtz researchers from different fields and have the opportunity to present your favorite tools and techniques. If you want to join, share your experience or have any question on the topic, let us and others know about it in [the meta pad](https://pad.gwdg.de/0HczFKgqS_C9L1QGzfpbJA#).

The number of available tools for software development are overwhelming and require us to know how to filter. Even Though there might not be a one line do it this way approach, its important to have a common baseline as a guide to select tools. Within this hacky hour, we will talk about different ways of selecting the right tools in software development

We are looking forward to seeing you!
