---
title: Software Carpentry Workshop
layout: event
organizers:
  - belayneh
  - dolling
lecturers:
  - belayneh
  - dolling
type:   workshop
start:
    date:   "2019-11-26"
    time:   "09:00"
end:
    date:   "2019-11-27"
    time:   "18:00"
location:
    campus: "Online Event"
fully_booked_out: true
registration_link: https://docs.google.com/forms/d/e/1FAIpQLScF37yq-HrE6JuGTz6zoFhAfGNowIE2Lr4e900IA0D_tmac2Q/closedform
registration_period:
    from:   "2019-10-30"
    to:     "2019-11-13"
excerpt:
    "This Software Carpentry workshop will teach Shell, Git and R programming for researchers."
redirect_to: https://hifis.net/events/2019/11/26/shell-git-R
---

## Goal

Introduce scientists and PhD students to a powerful toolset to enhance their
research software workflow.

## Content

This workshops contains the following topics:

* Introduction to _Shell_ scripting
* Using _Git_ as version control system (VCS)
* Introduction to the _R_ programming language

Details can also be found directly at the
[GFZ event page](https://swc-bb.git-pages.gfz-potsdam.de/swc-pages/2019-11-26-Potsdam-Berlin/).


## Requirements

No prior knowledge is required to attend this workshop.
