---
title: "Software Carpentry Workshop: Shell, Git and R programming"
layout: event
organizers:
  - belayneh
type:   workshop
start:
    date:   "2021-06-24"
    time:   "09:00"
end:
    date:   "2021-06-25"
    time:   "18:00"
location:
    campus: "Online"
excerpt:
    "This Software Carpentry workshop will teach Shell, Git and R programming for researchers."
redirect_from:
  - events/2021/03/16/2021-06-24and25-shell-git-R
  - events/2021/03/17/2021-06-24and25-shell-git-R
redirect_to: https://hifis.net/events/2021/06/24/shell-git-R
---

## Goal

Introduce scientists and PhD students to a powerful toolset to enhance their
research software workflow.

## Content

This workshops contains the following topics:

* Introduction to _Shell_ scripting
* Using _Git_ as version control system (VCS)
* Coding using the _R_ programming language


## Requirements

Basic knowledge of R progamming is required to attend this workshop.

Registration will open 2-3 weeks before the event. We are looking forward to seeing you!
