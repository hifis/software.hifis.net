---
title: "Helmholtz Hacky Hour #24"
layout: event
organizers:
  - dolling
type:      hacky-hour
start:
  date:   "2021-03-24"
  time:   "14:00"
end:
  time:   "15:00"
location:
  campus: "virtual"
  room:   "<a href=https://meet.gwdg.de/b/max-93j-2ef><i class=\"fas fa-external-link-alt\"></i> meet.gwdg.de</a>"
excerpt:  "<strong>My preferred data visualization tools</strong> There are hundreds of libraries and tools to solve that, but which one is the best?"
redirect_to: https://hifis.net/events/2021/03/24/helmholtz-hacky-hour-24
---
## My preferred data visualization tools
The Hacky Hour is intended to discuss and learn about different topics in the context of research software development. During the session, you will meet Helmholtz researchers from different fields and have the opportunity to present your favorite tools and techniques. If you want to join, share your experience or have any question on the topic, let us and others know about it in [the meta pad](https://pad.gwdg.de/0HczFKgqS_C9L1QGzfpbJA#).

Plots, graphs, maps - there are many opportunities for researchers to visualize data. For this session everyone is invited to present their favorite tool, give a brief overview or point out the advantages.

We are looking forward to seeing you!
