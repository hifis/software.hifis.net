---
title: "Helmholtz Hacky Hour #27"
layout: event
organizers:
  - dolling
type:      hacky-hour
start:
  date:   "2021-05-05"
  time:   "14:00"
end:
  time:   "15:00"
location:
  campus: "virtual"
  room:   "<a href=https://meet.gwdg.de/b/max-93j-2ef><i class=\"fas fa-external-link-alt\"></i> meet.gwdg.de</a>"
excerpt:  "<strong>Helmholtz services that can assist you in research software development</strong> These tools might make your daily work easier..."
redirect_to: https://hifis.net/events/2021/05/05/helmholtz-hacky-hour-27
---
## Helmholtz services that can assist you in research software development
The Hacky Hour is intended to discuss and learn about different topics in the context of research software development. During the session, you will meet Helmholtz researchers from different fields and have the opportunity to present your favorite tools and techniques. If you want to join, share your experience or have any question on the topic, let us and others know about it in [the meta pad](https://pad.gwdg.de/0HczFKgqS_C9L1QGzfpbJA#).

The Helmholtz Association provides various services and tools to help developing research software, improve collaborative workflows or tackle problems you didn't even know you had.

We are looking forward to seeing you!
