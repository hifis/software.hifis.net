---
title: Git with GitLab for Teams and Reproducible Research
layout: event
organizers:
  - belayneh
  - "Julia Neelmeijer"
lecturers:
  - belayneh
  - dolling
  - "Hannes Fuchs"
  - "Stefan Lüdtke"
  - "Christian Meeßen"
  - "Marc Hanisch"
  - brinckmann
type:   Workshop
start:
    date:   "2020-11-23"
    time:   "09:00"
end:
    date:   "2020-11-24"
    time:   "14:00"
location:
    campus: "Online Event"
registration_link: https://hifis-events.hzdr.de/event/31/
registration_period:
    from:   "2020-11-05"
    to:     "2020-11-16"
excerpt:
    "The workshop covers topics of Git with GitLab and best practices in software development in research."
redirect_to: https://hifis.net/events/2020/11/23/Git-with-GitLab-for-Teams-and-Reproducible-Research
---

## Goal

Having the knowledge and skills on how to use tools and techniques to preserve research output and to collaborate with peers benefit every research domain. This is a virtual workshop aiming to support researchers within a team to develop such skills. It is an interactive workshop including sessions from concepts to practice.


## Content

The workshop covers topics on: 
* Git with GitLab for individual use as well as for collaboration.
* Best practices in software development in research.


## Requirements

No prior knowledge or experiance is required.
