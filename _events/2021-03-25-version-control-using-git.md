---
title: Version Control using Git
layout: event
organizers:
  - belayneh
lecturers: 
  - belayneh
  - dolling
  - "Hannes Fuchs"
  - "Stefan Lüdtke"
  - "Christian Meeßen"
type:   workshop
start:
    date:   "2021-03-25"
    time:   "09:00"
end:
    date:   "2021-03-26"
    time:   "14:00"
location:
    campus: "Online"
fully_booked_out: false
registration_link: https://hifis-events.hzdr.de/event/72/
registration_period:
    from:   "2021-03-11"
    to:     "2021-03-17"
excerpt:
    "This workshop will teach version control using Git for scientists and PhD students."
redirect_from:
  - events/2021/03/16/2021-03-25and26-version-control-using-git
  - events/2021/03/17/2021-03-25and26-version-control-using-git
  - events/2021/03/18/2021-03-25and26-version-control-using-git
redirect_to: https://hifis.net/events/2021/03/25/version-control-using-git
---

## Goal

Introduce scientists and PhD students to the use of version control in research environment.

## Content

The workshop covers introductory topics on the use of version control in research, as well as hands on sessions on different functionalities of Git with GitLab. You will find more information on [the workshop page](https://swc-bb.git-pages.gfz-potsdam.de/swc-pages/2021-03-25-virtual/).


## Requirements

Neither prior knowledge nor experience in those tools is needed.
A headset (or at least headphones) is required.

