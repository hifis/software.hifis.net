---
title: "SORSE - A Series of Online Research Software Events" 
layout: event
organizers:
  - <a href="https://sorse.github.io/contact/">RSE community</a>
type: conference
start:
    date:   "2020-09-02"
end:
    date:   "2021-03-31"
excerpt:
    " A Series of Online Research Software Events organized by RSE community. "
redirect_to: https://hifis.net/events/2020/09/02/series-of-online-research-software-events
---

SORSE - A **S**eries of **O**nline **R**esearch **S**oftware **E**vents is
a replacement for many cancelled RSE conferences because of COVID-19. It aims to 
provide an opportunity for networking and discussion within all RSEs and anyone
involved with research software. Contribution is possible through
[this link](https://indico.scc.kit.edu/event/863/).
    
More information on the event can be found on the 
[event page](https://sorse.github.io/).

If you would like to **register** for the event, you can find more information about it [here](https://sorse.github.io/faq/howto/attendance).
