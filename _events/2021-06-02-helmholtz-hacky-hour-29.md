---
title: "Helmholtz Hacky Hour #29"
layout: event
organizers:
  - dworatzyk
type:      hacky-hour
start:
  date:   "2021-06-02"
  time:   "14:00"
end:
  time:   "15:00"
location:
  campus: "virtual"
  room:   "<a href=https://meet.gwdg.de/b/max-93j-2ef><i class=\"fas fa-external-link-alt\"></i> meet.gwdg.de</a>"
excerpt:  "<strong>Time & quality - a wicked problem</strong> How to decide when enough is enough"
redirect_to: https://hifis.net/events/2021/06/02/helmholtz-hacky-hour-29
---
## Time and quality in research software development - a toxic relationship
The Hacky Hour is intended to discuss and learn about different topics in the context of research software development. During the session, you will meet Helmholtz researchers from different fields and have the opportunity to present your favorite tools and techniques. If you want to join, share your experience or have any question on the topic, let us and others know about it in [the meta pad](https://pad.gwdg.de/0HczFKgqS_C9L1QGzfpbJA#).

In research software development quality is fundamental to produce valid and reliable results. On the other hand, we are struggling with limited time resources. Together we would like to discuss strategies of how to find the best tradeoff between high quality research software and meeting submission deadlines.

We are looking forward to seeing you!
