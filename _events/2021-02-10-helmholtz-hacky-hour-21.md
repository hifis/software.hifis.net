---
title: "Helmholtz Hacky Hour #21"
layout: event
organizers:
  - belayneh
type:      hacky-hour
start:
  date:   "2021-02-10"
  time:   "14:00"
end:
  time:   "15:00"
location:
  campus: "virtual"
  room:   "<a href=https://meet.gwdg.de/b/max-93j-2ef><i class=\"fas fa-external-link-alt\"></i> meet.gwdg.de</a>"
excerpt:  "<strong>Project organization for software development in research</strong> Important but often neglected in research -> project organization in software development"
redirect_to: https://hifis.net/events/2021/02/10/helmholtz-hacky-hour-21
---
## Project organization for software development in research
The Hacky Hour is intended to discuss and learn about different topics in the context of research software development. During the session, you will meet Helmholtz researchers from different fields and have the opportunity to present your favorite tools and techniques. If you want to join, share your experience or have any question on the topic, let us and others know about it in [the meta pad](https://pad.gwdg.de/0HczFKgqS_C9L1QGzfpbJA#).

Having a good understanding on how to organize projects can save time and often affect the quality of workflow in research. But, this requires a know-how on the basics and available tools. Within this Hacky Hour, we will discuss and share experiences  on different aspects of project organization

We are looking forward to seeing you!
