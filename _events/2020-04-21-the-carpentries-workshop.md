---
title: Software Carpentry Workshop
layout: event
organizers:
  - huste
  - erxleben
lecturers:
  - erxleben
  - huste
  - steinbach
type:   workshop
start:
    date:   "2020-04-22"
end:
    date:   "2020-04-23"
registration_link: https://hifis-events.hzdr.de/event/11/
location:
    campus: "Online Event"
fully_booked_out: false
registration_period:
    from:   "2020-04-07"
    to:     "2020-04-15"
excerpt:
    "This basic Software Carpentry workshop will teach Shell, Git
    and Python for scientists and PhD students."
redirect_to: https://hifis.net/events/2020/04/21/the-carpentries-workshop
---

## Goal

Introduce scientists and PhD students to a powerful toolset to enhance their
research software workflow.

## Content

A Software Carpentry workshop is conceptualized as a two-day event that covers
the basic tools required for a research software workflow:

* The _Shell_ as a foundation for the following tools
* Employing _Git_ as version control system (VCS)
* Introduction into the _Python_ programming language

Details and workshop materials can also be found directly at the
[Software Carpentries' lessons overview][swc-lessons].

## Requirements

Neither prior knowledge nor experience in those tools is needed.
Participants are asked to bring their own computer on which they can install
software.
It is recommended to read and follow the instructions on [how to set up the tools][setup-tools] for the workshop before the event.

[swc-lessons]: https://software-carpentry.org/lessons/
[setup-tools]: https://hifis.gitlab.io/2020-04-22-hzdr/#setup
